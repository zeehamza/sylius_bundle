<?php

namespace Sylius\Bundle\AdminBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * Time
 */
class Time implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $machineCode;

    /**
     * @var string
     */
    private $personCode;

    /**
     * @var int
     */
    private $jobNumber;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var time_immutable
     */
    private $startTime;

    /**
     * @var time_immutable
     */
    private $endTime;

    /**
     * @var int
     */
    private $qtyProduced;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set machineCode
     *
     * @param string $machineCode
     *
     * @return Time
     */
    public function setMachineCode($machineCode)
    {
        $this->machineCode = $machineCode;

        return $this;
    }

    /**
     * Get machineCode
     *
     * @return string
     */
    public function getMachineCode()
    {
        return $this->machineCode;
    }

    /**
     * Set personCode
     *
     * @param string $personCode
     *
     * @return Time
     */
    public function setPersonCode($personCode)
    {
        $this->personCode = $personCode;

        return $this;
    }

    /**
     * Get personCode
     *
     * @return string
     */
    public function getPersonCode()
    {
        return $this->personCode;
    }

    /**
     * Set jobNumber
     *
     * @param integer $jobNumber
     *
     * @return Time
     */
    public function setJobNumber($jobNumber)
    {
        $this->jobNumber = $jobNumber;

        return $this;
    }

    /**
     * Get jobNumber
     *
     * @return int
     */
    public function getJobNumber()
    {
        return $this->jobNumber;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Time
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set startTime
     *
     * @param time_immutable $startTime
     *
     * @return Time
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return time_immutable
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param time_immutable $endTime
     *
     * @return Time
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return time_immutable
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set qtyProduced
     *
     * @param integer $qtyProduced
     *
     * @return Time
     */
    public function setQtyProduced($qtyProduced)
    {
        $this->qtyProduced = $qtyProduced;

        return $this;
    }

    /**
     * Get qtyProduced
     *
     * @return int
     */
    public function getQtyProduced()
    {
        return $this->qtyProduced;
    }
}

