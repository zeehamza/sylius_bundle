<?php

namespace Sylius\Bundle\AdminBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * JobAttributes
 */
class JobAttributes implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $jobNumber;

    /**
     * @var string
     */
    private $productCode;

    /**
     * @var string
     */
    private $attrName;

    /**
     * @var string
     */
    private $attrValue;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jobNumber
     *
     * @param integer $jobNumber
     *
     * @return JobAttributes
     */
    public function setJobNumber($jobNumber)
    {
        $this->jobNumber = $jobNumber;

        return $this;
    }

    /**
     * Get jobNumber
     *
     * @return int
     */
    public function getJobNumber()
    {
        return $this->jobNumber;
    }

    /**
     * Set productCode
     *
     * @param string $productCode
     *
     * @return JobAttributes
     */
    public function setProductCode($productCode)
    {
        $this->productCode = $productCode;

        return $this;
    }

    /**
     * Get productCode
     *
     * @return string
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * Set attrName
     *
     * @param string $attrName
     *
     * @return JobAttributes
     */
    public function setAttrName($attrName)
    {
        $this->attrName = $attrName;

        return $this;
    }

    /**
     * Get attrName
     *
     * @return string
     */
    public function getAttrName()
    {
        return $this->attrName;
    }

    /**
     * Set attrValue
     *
     * @param string $attrValue
     *
     * @return JobAttributes
     */
    public function setAttrValue($attrValue)
    {
        $this->attrValue = $attrValue;

        return $this;
    }

    /**
     * Get attrValue
     *
     * @return string
     */
    public function getAttrValue()
    {
        return $this->attrValue;
    }
}

