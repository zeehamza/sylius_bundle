<?php

namespace Sylius\Bundle\AdminBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * Material
 */
class Material implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $materialCode;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $matGroup;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $minimumStock;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $averagePrice;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set materialCode
     *
     * @param string $materialCode
     *
     * @return Material
     */
    public function setMaterialCode($materialCode)
    {
        $this->materialCode = $materialCode;

        return $this;
    }

    /**
     * Get materialCode
     *
     * @return string
     */
    public function getMaterialCode()
    {
        return $this->materialCode;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Material
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set matGroup
     *
     * @param string $matGroup
     *
     * @return Material
     */
    public function setMatGroup($matGroup)
    {
        $this->matGroup = $matGroup;

        return $this;
    }

    /**
     * Get matGroup
     *
     * @return string
     */
    public function getMatGroup()
    {
        return $this->matGroup;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Material
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set minimumStock
     *
     * @param string $minimumStock
     *
     * @return Material
     */
    public function setMinimumStock($minimumStock)
    {
        $this->minimumStock = $minimumStock;

        return $this;
    }

    /**
     * Get minimumStock
     *
     * @return string
     */
    public function getMinimumStock()
    {
        return $this->minimumStock;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Material
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set averagePrice
     *
     * @param string $averagePrice
     *
     * @return Material
     */
    public function setAveragePrice($averagePrice)
    {
        $this->averagePrice = $averagePrice;

        return $this;
    }

    /**
     * Get averagePrice
     *
     * @return string
     */
    public function getAveragePrice()
    {
        return $this->averagePrice;
    }
}

