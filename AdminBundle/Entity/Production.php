<?php

namespace Sylius\Bundle\AdminBundle\Entity;

/**
 * Production
 */
class Production
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $jobNumber;

    /**
     * @var string
     */
    private $product;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @var \DateTime
     */
    private $dateProduced;

    /**
     * @var time_immutable
     */
    private $timeProduced;

    /**
     * @var string
     */
    private $personCode;

    /**
     * @var string
     */
    private $type;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jobNumber
     *
     * @param integer $jobNumber
     *
     * @return Production
     */
    public function setJobNumber($jobNumber)
    {
        $this->jobNumber = $jobNumber;

        return $this;
    }

    /**
     * Get jobNumber
     *
     * @return int
     */
    public function getJobNumber()
    {
        return $this->jobNumber;
    }

    /**
     * Set product
     *
     * @param string $product
     *
     * @return Production
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return Production
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set dateProduced
     *
     * @param \DateTime $dateProduced
     *
     * @return Production
     */
    public function setDateProduced($dateProduced)
    {
        $this->dateProduced = $dateProduced;

        return $this;
    }

    /**
     * Get dateProduced
     *
     * @return \DateTime
     */
    public function getDateProduced()
    {
        return $this->dateProduced;
    }

    /**
     * Set timeProduced
     *
     * @param time_immutable $timeProduced
     *
     * @return Production
     */
    public function setTimeProduced($timeProduced)
    {
        $this->timeProduced = $timeProduced;

        return $this;
    }

    /**
     * Get timeProduced
     *
     * @return time_immutable
     */
    public function getTimeProduced()
    {
        return $this->timeProduced;
    }

    /**
     * Set personCode
     *
     * @param string $personCode
     *
     * @return Production
     */
    public function setPersonCode($personCode)
    {
        $this->personCode = $personCode;

        return $this;
    }

    /**
     * Get personCode
     *
     * @return string
     */
    public function getPersonCode()
    {
        return $this->personCode;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Production
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}

