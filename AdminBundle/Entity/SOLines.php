<?php

namespace Sylius\Bundle\AdminBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * SOLines
 */
class SOLines implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $sONumber;

    /**
     * @var int
     */
    private $jobNumber;

    /**
     * @var string
     */
    private $productCode;

    /**
     * @var string
     */
    private $quantityOrdered;

    /**
     * @var string
     */
    private $priceUnit;

    /**
     * @var string
     */
    private $status;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sONumber
     *
     * @param integer $sONumber
     *
     * @return SOLines
     */
    public function setSONumber($sONumber)
    {
        $this->sONumber = $sONumber;

        return $this;
    }

    /**
     * Get sONumber
     *
     * @return int
     */
    public function getSONumber()
    {
        return $this->sONumber;
    }

    /**
     * Set jobNumber
     *
     * @param integer $jobNumber
     *
     * @return SOLines
     */
    public function setJobNumber($jobNumber)
    {
        $this->jobNumber = $jobNumber;

        return $this;
    }

    /**
     * Get jobNumber
     *
     * @return int
     */
    public function getJobNumber()
    {
        return $this->jobNumber;
    }

    /**
     * Set productCode
     *
     * @param string $productCode
     *
     * @return SOLines
     */
    public function setProductCode($productCode)
    {
        $this->productCode = $productCode;

        return $this;
    }

    /**
     * Get productCode
     *
     * @return string
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * Set quantityOrdered
     *
     * @param string $quantityOrdered
     *
     * @return SOLines
     */
    public function setQuantityOrdered($quantityOrdered)
    {
        $this->quantityOrdered = $quantityOrdered;

        return $this;
    }

    /**
     * Get quantityOrdered
     *
     * @return string
     */
    public function getQuantityOrdered()
    {
        return $this->quantityOrdered;
    }

    /**
     * Set priceUnit
     *
     * @param string $priceUnit
     *
     * @return SOLines
     */
    public function setPriceUnit($priceUnit)
    {
        $this->priceUnit = $priceUnit;

        return $this;
    }

    /**
     * Get priceUnit
     *
     * @return string
     */
    public function getPriceUnit()
    {
        return $this->priceUnit;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return SOLines
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}

