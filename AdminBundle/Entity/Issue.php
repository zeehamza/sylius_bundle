<?php

namespace Sylius\Bundle\AdminBundle\Entity;

/**
 * Issue
 */
class Issue
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $jobNumber;

    /**
     * @var string
     */
    private $materialCode;

    /**
     * @var string
     */
    private $quantity;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var time_immutable
     */
    private $time;

    /**
     * @var string
     */
    private $personCode;

    /**
     * @var string
     */
    private $type;

    /**
     * @var int
     */
    private $issueNumber;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jobNumber
     *
     * @param integer $jobNumber
     *
     * @return Issue
     */
    public function setJobNumber($jobNumber)
    {
        $this->jobNumber = $jobNumber;

        return $this;
    }

    /**
     * Get jobNumber
     *
     * @return int
     */
    public function getJobNumber()
    {
        return $this->jobNumber;
    }

    /**
     * Set materialCode
     *
     * @param string $materialCode
     *
     * @return Issue
     */
    public function setMaterialCode($materialCode)
    {
        $this->materialCode = $materialCode;

        return $this;
    }

    /**
     * Get materialCode
     *
     * @return string
     */
    public function getMaterialCode()
    {
        return $this->materialCode;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     *
     * @return Issue
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Issue
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set time
     *
     * @param time_immutable $time
     *
     * @return Issue
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return time_immutable
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set personCode
     *
     * @param string $personCode
     *
     * @return Issue
     */
    public function setPersonCode($personCode)
    {
        $this->personCode = $personCode;

        return $this;
    }

    /**
     * Get personCode
     *
     * @return string
     */
    public function getPersonCode()
    {
        return $this->personCode;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Issue
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set issueNumber
     *
     * @param integer $issueNumber
     *
     * @return Issue
     */
    public function setIssueNumber($issueNumber)
    {
        $this->issueNumber = $issueNumber;

        return $this;
    }

    /**
     * Get issueNumber
     *
     * @return int
     */
    public function getIssueNumber()
    {
        return $this->issueNumber;
    }
}

