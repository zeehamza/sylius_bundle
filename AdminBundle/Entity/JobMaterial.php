<?php

namespace Sylius\Bundle\AdminBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * JobMaterial
 */
class JobMaterial implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $jobNumber;

    /**
     * @var string
     */
    private $materialCode;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $quantity;

    /**
     * @var string
     */
    private $instruction;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jobNumber
     *
     * @param integer $jobNumber
     *
     * @return JobMaterial
     */
    public function setJobNumber($jobNumber)
    {
        $this->jobNumber = $jobNumber;

        return $this;
    }

    /**
     * Get jobNumber
     *
     * @return int
     */
    public function getJobNumber()
    {
        return $this->jobNumber;
    }

    /**
     * Set materialCode
     *
     * @param string $materialCode
     *
     * @return JobMaterial
     */
    public function setMaterialCode($materialCode)
    {
        $this->materialCode = $materialCode;

        return $this;
    }

    /**
     * Get materialCode
     *
     * @return string
     */
    public function getMaterialCode()
    {
        return $this->materialCode;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return JobMaterial
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     *
     * @return JobMaterial
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set instruction
     *
     * @param string $instruction
     *
     * @return JobMaterial
     */
    public function setInstruction($instruction)
    {
        $this->instruction = $instruction;

        return $this;
    }

    /**
     * Get instruction
     *
     * @return string
     */
    public function getInstruction()
    {
        return $this->instruction;
    }
}

