<?php

namespace Sylius\Bundle\AdminBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * Job
 */
class Job implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    
    private $jobNumber;

    /**
     * @var int
     */
    private $sONumber;

    /**
     * @var \DateTime
     */
    private $dateRequired;

    /**
     * @var string
     */
    private $product;

    /**
     * @var string
     */
    private $quantity;

    /**
     * @var string
     */
    private $status;

    /**
     * @var int
     */
    private $linkJobNumber ;

    /**
     * @var string
     */
    private $artworkAddress;

    /**
     * @var string
     */
    private $materialCost;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jobNumber
     *
     * @param integer $jobNumber
     *
     * @return Job
     */
    
    public function setJobNumber($jobNumber)
    {
        $this->jobNumber = $jobNumber;

        return $this;
    }

    /**
     * Get jobNumber
     *
     * @return int
     */
    public function getJobNumber()
    {
        return $this->jobNumber;
    }

    /**
     * Set sONumber
     *
     * @param integer $sONumber
     *
     * @return Job
     */
    public function setSONumber($sONumber)
    {
        $this->sONumber = $sONumber;

        return $this;
    }

    /**
     * Get sONumber
     *
     * @return int
     */
    public function getSONumber()
    {
        return $this->sONumber;
    }

    /**
     * Set dateRequired
     *
     * @param \DateTime $dateRequired
     *
     * @return Job
     */
    public function setDateRequired($dateRequired)
    {
        $this->dateRequired = $dateRequired;

        return $this;
    }

    /**
     * Get dateRequired
     *
     * @return \DateTime
     */
    public function getDateRequired()
    {
        return $this->dateRequired;
    }

    /**
     * Set product
     *
     * @param string $product
     *
     * @return Job
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     *
     * @return Job
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Job
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set linkJobNumber 
     *
     * @param integer $linkJobNumber 
     *
     * @return Job
     */
    public function setlinkJobNumber ($linkJobNumber )
    {
        $this->linkJobNumber  = $linkJobNumber ;

        return $this;
    }

    /**
     * Get linkJobNumber 
     *
     * @return int
     */
    public function getlinkJobNumber ()
    {
        return $this->linkJobNumber ;
    }

    /**
     * Set artworkAddress
     *
     * @param string $artworkAddress
     *
     * @return Job
     */
    public function setArtworkAddress($artworkAddress)
    {
        $this->artworkAddress = $artworkAddress;

        return $this;
    }

    /**
     * Get artworkAddress
     *
     * @return string
     */
    public function getArtworkAddress()
    {
        return $this->artworkAddress;
    }

    /**
     * Set materialCost
     *
     * @param string $materialCost
     *
     * @return Job
     */
    public function setMaterialCost($materialCost)
    {
        $this->materialCost = $materialCost;

        return $this;
    }

    /**
     * Get materialCost
     *
     * @return string
     */
    public function getMaterialCost()
    {
        return $this->materialCost;
    }
}

