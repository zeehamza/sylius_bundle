<?php

namespace Sylius\Bundle\AdminBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * JobRoute
 */
class JobRoute implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $jobNumber;

    /**
     * @var int
     */
    private $sequence;

    /**
     * @var string
     */
    private $machineCode;

    /**
     * @var int
     */
    private $minutes;

    /**
     * @var string
     */
    private $instruction;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jobNumber
     *
     * @param integer $jobNumber
     *
     * @return JobRoute
     */
    public function setJobNumber($jobNumber)
    {
        $this->jobNumber = $jobNumber;

        return $this;
    }

    /**
     * Get jobNumber
     *
     * @return int
     */
    public function getJobNumber()
    {
        return $this->jobNumber;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     *
     * @return JobRoute
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence
     *
     * @return int
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set machineCode
     *
     * @param string $machineCode
     *
     * @return JobRoute
     */
    public function setMachineCode($machineCode)
    {
        $this->machineCode = $machineCode;

        return $this;
    }

    /**
     * Get machineCode
     *
     * @return string
     */
    public function getMachineCode()
    {
        return $this->machineCode;
    }

    /**
     * Set minutes
     *
     * @param integer $minutes
     *
     * @return JobRoute
     */
    public function setMinutes($minutes)
    {
        $this->minutes = $minutes;

        return $this;
    }

    /**
     * Get minutes
     *
     * @return int
     */
    public function getMinutes()
    {
        return $this->minutes;
    }

    /**
     * Set instruction
     *
     * @param string $instruction
     *
     * @return JobRoute
     */
    public function setInstruction($instruction)
    {
        $this->instruction = $instruction;

        return $this;
    }

    /**
     * Get instruction
     *
     * @return string
     */
    public function getInstruction()
    {
        return $this->instruction;
    }
}

