<?php

namespace Sylius\Bundle\AdminBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * Receipt
 */
class Receipt implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $receiptNumber;

    /**
     * @var int
     */
    private $pONumber;

    /**
     * @var string
     */
    private $materialCode;

    /**
     * @var string
     */
    private $quantity;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var time_immutable
     */
    private $time;

    /**
     * @var string
     */
    private $person;

    /**
     * @var string
     */
    private $type;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set receiptNumber
     *
     * @param integer $receiptNumber
     *
     * @return Receipt
     */
    public function setReceiptNumber($receiptNumber)
    {
        $this->receiptNumber = $receiptNumber;

        return $this;
    }

    /**
     * Get receiptNumber
     *
     * @return int
     */
    public function getReceiptNumber()
    {
        return $this->receiptNumber;
    }

    /**
     * Set pONumber
     *
     * @param integer $pONumber
     *
     * @return Receipt
     */
    public function setPONumber($pONumber)
    {
        $this->pONumber = $pONumber;

        return $this;
    }

    /**
     * Get pONumber
     *
     * @return int
     */
    public function getPONumber()
    {
        return $this->pONumber;
    }

    /**
     * Set materialCode
     *
     * @param string $materialCode
     *
     * @return Receipt
     */
    public function setMaterialCode($materialCode)
    {
        $this->materialCode = $materialCode;

        return $this;
    }

    /**
     * Get materialCode
     *
     * @return string
     */
    public function getMaterialCode()
    {
        return $this->materialCode;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     *
     * @return Receipt
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Receipt
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set time
     *
     * @param time_immutable $time
     *
     * @return Receipt
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return time_immutable
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set person
     *
     * @param string $person
     *
     * @return Receipt
     */
    public function setPerson($person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return string
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Receipt
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}

