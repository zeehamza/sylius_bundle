<?php

namespace Sylius\Bundle\AdminBundle\Entity;

use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * ReelMaterial
 */
class ReelMaterial implements ResourceInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $materialCode;

    /**
     * @var int
     */
    private $gSM;

    /**
     * @var int
     */
    private $width;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set materialCode
     *
     * @param string $materialCode
     *
     * @return ReelMaterial
     */
    public function setMaterialCode($materialCode)
    {
        $this->materialCode = $materialCode;

        return $this;
    }

    /**
     * Get materialCode
     *
     * @return string
     */
    public function getMaterialCode()
    {
        return $this->materialCode;
    }

    /**
     * Set gSM
     *
     * @param integer $gSM
     *
     * @return ReelMaterial
     */
    public function setGSM($gSM)
    {
        $this->gSM = $gSM;

        return $this;
    }

    /**
     * Get gSM
     *
     * @return int
     */
    public function getGSM()
    {
        return $this->gSM;
    }

    /**
     * Set width
     *
     * @param integer $width
     *
     * @return ReelMaterial
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }
}

