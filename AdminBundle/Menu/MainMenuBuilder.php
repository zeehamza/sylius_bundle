<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Paweł Jędrzejewski
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Sylius\Bundle\AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * @author Paweł Jędrzejewski <pawel@sylius.org>
 */
final class MainMenuBuilder
{
    public const EVENT_NAME = 'sylius.menu.admin.main';

    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param FactoryInterface $factory
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(FactoryInterface $factory, EventDispatcherInterface $eventDispatcher)
    {
        $this->factory = $factory;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param array $options
     *
     * @return ItemInterface
     */
    public function createMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        $this->addCatalogSubMenu($menu);
        $this->addSalesSubMenu($menu);
        $this->addSupplierSubMenu($menu);
        $this->addPersonSubMenu($menu);
        $this->addDeliverySubMenu($menu);
        $this->addMaterialSubMenu($menu);
        $this->addReceiptSubMenu($menu);
        $this->addJobSubMenu($menu);
        $this->addSiteSubMenu($menu);
        $this->addCustomersSubMenu($menu);
        $this->addMarketingSubMenu($menu);
        $this->addConfigurationSubMenu($menu);
        
        

        $this->eventDispatcher->dispatch(self::EVENT_NAME, new MenuBuilderEvent($this->factory, $menu));

        return $menu;
    }

    /**
     * @param ItemInterface $menu
     */
    private function addCatalogSubMenu(ItemInterface $menu): void
    {
        $catalog = $menu
            ->addChild('catalog')
            ->setLabel('sylius.menu.admin.main.catalog.header')
        ;

        $catalog
            ->addChild('taxons', ['route' => 'sylius_admin_taxon_create'])
            ->setLabel('sylius.menu.admin.main.catalog.taxons')
            ->setLabelAttribute('icon', 'folder')
        ;

        $catalog
            ->addChild('products', ['route' => 'sylius_admin_product_index'])
            ->setLabel('sylius.menu.admin.main.catalog.products')
            ->setLabelAttribute('icon', 'cube')
        ;

        $catalog
            ->addChild('inventory', ['route' => 'sylius_admin_inventory_index'])
            ->setLabel('sylius.menu.admin.main.catalog.inventory')
            ->setLabelAttribute('icon', 'history')
        ;

        $catalog
            ->addChild('attributes', ['route' => 'sylius_admin_product_attribute_index'])
            ->setLabel('sylius.menu.admin.main.catalog.attributes')
            ->setLabelAttribute('icon', 'cubes')
        ;

        $catalog
            ->addChild('options', ['route' => 'sylius_admin_product_option_index'])
            ->setLabel('sylius.menu.admin.main.catalog.options')
            ->setLabelAttribute('icon', 'options')
        ;

        $catalog
            ->addChild('association_types', ['route' => 'sylius_admin_product_association_type_index'])
            ->setLabel('sylius.menu.admin.main.catalog.association_types')
            ->setLabelAttribute('icon', 'tasks')
        ;
    }

    /**
     * @param ItemInterface $menu
     */
    private function addCustomersSubMenu(ItemInterface $menu): void
    {
        $customers = $menu
            ->addChild('customers')
            ->setLabel('sylius.menu.admin.main.customers.header')
        ;

        $customers
            ->addChild('customers', ['route' => 'sylius_admin_customer_index'])
            ->setLabel('sylius.menu.admin.main.customers.customers')
            ->setLabelAttribute('icon', 'users')
        ;

        $customers
            ->addChild('groups', ['route' => 'sylius_admin_customer_group_index'])
            ->setLabel('sylius.menu.admin.main.customers.groups')
            ->setLabelAttribute('icon', 'archive')
        ;
    }

    /**
     * @param ItemInterface $menu
     */
    private function addMarketingSubMenu(ItemInterface $menu): void
    {
        $marketing = $menu
            ->addChild('marketing')
            ->setLabel('sylius.menu.admin.main.marketing.header')
        ;

        $marketing
            ->addChild('promotions', ['route' => 'sylius_admin_promotion_index'])
            ->setLabel('sylius.menu.admin.main.marketing.promotions')
            ->setLabelAttribute('icon', 'in cart')
        ;

        $marketing
            ->addChild('product_reviews', ['route' => 'sylius_admin_product_review_index'])
            ->setLabel('sylius.menu.admin.main.marketing.product_reviews')
            ->setLabelAttribute('icon', 'newspaper')
        ;
    }

    /**
     * @param ItemInterface $menu
     */
    private function addSalesSubMenu(ItemInterface $menu): void
    {
        $sales = $menu
            ->addChild('sales')
            ->setLabel('sylius.menu.admin.main.sales.header')
        ;

        $sales
            ->addChild('orders', ['route' => 'sylius_admin_order_index'])
            ->setLabel('sylius.menu.admin.main.sales.orders')
            ->setLabelAttribute('icon', 'cart')
        ;

        $sales
            ->addChild('Attributes', ['route' => 'sylius_admin_soattributes_index'])
            ->setLabel('Attributes')
            ->setLabelAttribute('icon', 'cart')
        ;

        $sales
            ->addChild('Lines', ['route' => 'sylius_admin_solines_index'])
            ->setLabel('Lines')
            ->setLabelAttribute('icon', 'cart')
        ;
    }

    /**
     * @param ItemInterface $menu
     */
    private function addConfigurationSubMenu(ItemInterface $menu): void
    {
        $configuration = $menu
            ->addChild('configuration')
            ->setLabel('sylius.menu.admin.main.configuration.header')
        ;

        $configuration
            ->addChild('channels', ['route' => 'sylius_admin_channel_index'])
            ->setLabel('sylius.menu.admin.main.configuration.channels')
            ->setLabelAttribute('icon', 'random')
        ;

        $configuration
            ->addChild('countries', ['route' => 'sylius_admin_country_index'])
            ->setLabel('sylius.menu.admin.main.configuration.countries')
            ->setLabelAttribute('icon', 'flag')
        ;

        $configuration
            ->addChild('zones', ['route' => 'sylius_admin_zone_index'])
            ->setLabel('sylius.menu.admin.main.configuration.zones')
            ->setLabelAttribute('icon', 'world')
        ;

        $configuration
            ->addChild('currencies', ['route' => 'sylius_admin_currency_index'])
            ->setLabel('sylius.menu.admin.main.configuration.currencies')
            ->setLabelAttribute('icon', 'dollar')
        ;

        $configuration
            ->addChild('exchange_rates', ['route' => 'sylius_admin_exchange_rate_index'])
            ->setLabel('sylius.menu.admin.main.configuration.exchange_rates')
            ->setLabelAttribute('icon', 'sliders')
        ;

        $configuration
            ->addChild('locales', ['route' => 'sylius_admin_locale_index'])
            ->setLabel('sylius.menu.admin.main.configuration.locales')
            ->setLabelAttribute('icon', 'translate')
        ;

        $configuration
            ->addChild('payment_methods', ['route' => 'sylius_admin_payment_method_index'])
            ->setLabel('sylius.menu.admin.main.configuration.payment_methods')
            ->setLabelAttribute('icon', 'payment')
        ;

        $configuration
            ->addChild('shipping_methods', ['route' => 'sylius_admin_shipping_method_index'])
            ->setLabel('sylius.menu.admin.main.configuration.shipping_methods')
            ->setLabelAttribute('icon', 'shipping')
        ;

        $configuration
            ->addChild('shipping_categories', ['route' => 'sylius_admin_shipping_category_index'])
            ->setLabel('sylius.menu.admin.main.configuration.shipping_categories')
            ->setLabelAttribute('icon', 'list layout')
        ;

        $configuration
            ->addChild('tax_categories', ['route' => 'sylius_admin_tax_category_index'])
            ->setLabel('sylius.menu.admin.main.configuration.tax_categories')
            ->setLabelAttribute('icon', 'tags')
        ;

        $configuration
            ->addChild('tax_rates', ['route' => 'sylius_admin_tax_rate_index'])
            ->setLabel('sylius.menu.admin.main.configuration.tax_rates')
            ->setLabelAttribute('icon', 'money')
        ;

        $configuration
            ->addChild('admin_users', ['route' => 'sylius_admin_admin_user_index'])
            ->setLabel('sylius.menu.admin.main.configuration.admin_users')
            ->setLabelAttribute('icon', 'lock')
        ;
    }


    private function addSupplierSubMenu(ItemInterface $menu): void
    {
        $supplier = $menu
            ->addChild('suppliers')
            ->setLabel('Suppliers')
        ;

      $supplier
            ->addChild('suppliers', ['route' => 'sylius_admin_supplier_index'])
            ->setLabel('Suppliers')
            ->setLabelAttribute('icon', 'users')
        ;

        $supplier
            ->addChild('suppliersaddress', ['route' => 'sylius_admin_supplier_index'])
            ->setLabel('Suppliers Address')
            ->setLabelAttribute('icon', 'users')
        ;
    }

    private function addPersonSubMenu(ItemInterface $menu): void
    {
        $Persons = $menu
            ->addChild('Persons')
            ->setLabel('Persons')
        ;

      $Persons
            ->addChild('persons', ['route' => 'sylius_admin_person_index'])
            ->setLabel('Persons')
            ->setLabelAttribute('icon', 'users')
        ;
    }

    private function addDeliverySubMenu(ItemInterface $menu): void
    {
        $Delivery = $menu
            ->addChild('Delivery')
            ->setLabel('Delivery')
        ;

      $Delivery
            ->addChild('Delivery', ['route' => 'sylius_admin_delivery_index'])
            ->setLabel('Delivery')
            ->setLabelAttribute('icon', 'cart')
        ;
    }

    private function addMaterialSubMenu(ItemInterface $menu): void
    {
        $Material = $menu
            ->addChild('Material')
            ->setLabel('Material')
        ;

        $Material
            ->addChild('Material', ['route' => 'sylius_admin_material_index'])
            ->setLabel('Material')
            ->setLabelAttribute('icon', 'industry')
        ;

        $Material
            ->addChild('matgroup', ['route' => 'sylius_admin_matgroup_index'])
            ->setLabel('Material Group')
            ->setLabelAttribute('icon', 'group')
        ;

        $Material
            ->addChild('sheetmaterial', ['route' => 'sylius_admin_sheetmaterial_index'])
            ->setLabel('Sheet Material')
            ->setLabelAttribute('icon', 'industry')
        ;

        $Material
            ->addChild('reelmaterial', ['route' => 'sylius_admin_reelmaterial_index'])
            ->setLabel('Reel Material')
            ->setLabelAttribute('icon', 'industry')
        ;

        $Material
            ->addChild('Time', ['route' => 'sylius_admin_time_index'])
            ->setLabel('Time')
            ->setLabelAttribute('icon', 'time')
        ;

    }

     private function addReceiptSubMenu(ItemInterface $menu): void
    {
        $Material = $menu
            ->addChild('Receipt')
            ->setLabel('Receipt')
        ;

      $Material
            ->addChild('Receipt', ['route' => 'sylius_admin_receipt_index'])
            ->setLabel('Receipt')
            ->setLabelAttribute('icon', 'file')
        ;
    } 

    private function addJobSubMenu(ItemInterface $menu): void
    {
        $Job = $menu
            ->addChild('Job')
            ->setLabel('Job')
        ;

        $Job
            ->addChild('Job', ['route' => 'sylius_admin_job_index'])
            ->setLabel('Job')
            ->setLabelAttribute('icon', 'briefcase')
            ;
        $Job
            ->addChild('jobmaterial', ['route' => 'sylius_admin_jobmaterial_index'])
            ->setLabel('Job Material')
            ->setLabelAttribute('icon', 'briefcase')
            ;
        $Job
            ->addChild('jobattributes', ['route' => 'sylius_admin_jobattributes_index'])
            ->setLabel('Job Attributes')
            ->setLabelAttribute('icon', 'briefcase')
            ;
        $Job
            ->addChild('jobroute', ['route' => 'sylius_admin_jobroute_index'])
            ->setLabel('Job Route')
            ->setLabelAttribute('icon', 'briefcase')
            ;
    }

    private function addSiteSubMenu(ItemInterface $menu): void
    {
        $supplier = $menu
            ->addChild('setting')
            ->setLabel('Settings')
        ;

      $supplier
            ->addChild('setting', ['route' => 'sylius_admin_supplier_index'])
            ->setLabel('Site Setting')
            ->setLabelAttribute('icon', 'options')
        ;

    }
}
