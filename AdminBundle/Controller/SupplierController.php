<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Paweł Jędrzejewski
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Sylius\Bundle\AdminBundle\Controller;

//use Sylius\Component\Core\Customer\Statistics\CustomerStatisticsProviderInterface;
//use Sylius\Component\Core\Model\CustomerInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @author Jan Góralski <jan.goralski@lakion.com>
 */
final class SupplierController
{
    /**
     * @var SupplierStatisticsProviderInterface
     */
    //private $statisticsProvider;

    /**
     * @var RepositoryInterface
     */
    //private $supplierRepository;

    /**
     * @var EngineInterface
     */
    private $templatingEngine;

    /**
     * @param SupplierStatisticsProviderInterface $statisticsProvider
     * @param RepositoryInterface $supplierRepository
     * @param EngineInterface $templatingEngine
     */
     public function __construct(
        //SupplierStatisticsProviderInterface $statisticsProvider,
       // RepositoryInterface $supplierRepository,
        EngineInterface $templatingEngine
    ) {
        //$this->statisticsProvider = $statisticsProvider;
        //$this->supplierRepository = $supplierRepository;
        $this->templatingEngine = $templatingEngine;
    } 

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @throws HttpException
     */


    /* public function indexAction(): Response
    {
        return $this->templatingEngine->renderResponse(
            '@SyliusAdmin/supplier/index.html.twig');
    } */

    public function showAction(): Response
    {
        return $this->templatingEngine->renderResponse(
            '@SyliusAdmin/supplier/index.html.twig');
    }

    public function renderAction(Request $request): Response
    {
        $supplierId = $request->query->get('supplierId');

        /** @var supplierInterface $supplier */
        $supplier = $this->supplierRepository->find($supplierId);
        if (null === $supplier) {
            throw new HttpException(
                Response::HTTP_BAD_REQUEST,
                sprintf('supplier with id %s doesn\'t exist.', $supplierId)
            );
        }

        $supplierStatistics = $this->statisticsProvider->getsupplierStatistics($supplier);

        return $this->templatingEngine->renderResponse(
            '@SyliusAdmin/supplier/Show/Statistics/index.html.twig',
            ['statistics' => $supplierStatistics]
        );
    }
}
